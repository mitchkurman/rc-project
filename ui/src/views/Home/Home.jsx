import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Demographics } from 'views/Demographics/Demographics.jsx'
import { Offers } from 'views/Offers/Offers.jsx';
import { Transactions } from "views/Transactions/Transactions.jsx"
import { CardNumberSearch } from "views/CardNumberSearch/CardNumberSearch.jsx"


class Home extends Component {
  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }
  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col lg={12}>
              <CardNumberSearch />
            </Col>
          </Row>
          <Row>
            <Col lg={6}>
              <Demographics />
            </Col>

            <Col lg={6}>
              <Offers />
            </Col>
          </Row>
          <Row>
            <Col lg={12}>
              <Transactions />
            </Col>
          </Row>


        </Grid>
      </div>
    );
  }
}

export default Home;
