import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";

import { getCustomerInfo } from 'actions/index.js';
import { parseAddress, parseLocation } from 'parse-address';
import { Card } from "components/Card/Card.jsx";
import { view } from 'react-easy-state';
import { store }  from '../../appStore.js';


export class Demographics extends Component {

  componentDidMount(){
    getCustomerInfo("372545000000010").then((response) => {
          console.log('inside api call');
          console.log(response);

          if(response.status !== 200){
            throw Error(response.statusText);
          }
         console.log('about to call set customer in demo.jsx');
         store.setCustomer(response.data);
          console.log(store);

          this.forceUpdate();
        })
        .catch((e) => alert(e))
  }


  render() {
    console.log(store);
    const {
      getCustomer,
      getEmployeeId,
      getEmployeeName,
      customer,
      cardnumber
    } = store
    // var n = {store.customer.map((t) => t.firstName)};
    console.log(store.getCustomer);
    return (
      <Card
        id="customerInfo"
        title="Customer Details"
        category=
        {
            store.customer.map((prop,key) =>
            {
              // console.log('cust');
              // console.log(prop.firstInitial);
              return(
                prop.cardnumber
              )
            })
          }
        content={
          <div>
            <Row>
              <Col md={6}>
                  Customer Name:
              </Col>
              <Col md={6}>
              {
                  store.customer.map((prop,key) =>
                  {
                    // console.log(prop.firstInitial);
                    return(
                      prop.firstInitial + ' ' + prop.lastName
                    )
                  })
                }
              </Col>
            </Row>
            <Row><Col md={6}><br/></Col><Col md={6}></Col></Row>
            <Row>
              <Col md={6}>
                  Address:
              </Col>
              <Col md={6}>
              {
                  store.customer.map((prop,key) =>
                  {
                    var tmp = parseAddress(prop.address);
                    return(
                      <div key={key}>
                          {tmp.number + ' ' + tmp.prefix + ' ' + tmp.street + ' ' + tmp.type}<br/>
                          {tmp.city+', '+tmp.state }<br />
                          {tmp.zip}
                      </div>
                    )
                  })
                }
              </Col>
            </Row>
            <Row><Col md={6}><br/></Col><Col md={6}></Col></Row>
            <Row>
              <Col md={6}>
                  Phone Number:
              </Col>
              <Col md={6}>
              {
                  store.customer.map((prop,key) =>
                  {
                    return(
                      <div key={key}>
                          {prop.phone}
                      </div>
                    )
                    // return prop.transaction_id;
                  })
                }
              </Col>
            </Row>
          </div>
      }
    />
  )}
}
export default view(Demographics);
