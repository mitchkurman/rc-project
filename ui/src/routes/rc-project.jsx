// import Dashboard from "views/Dashboard/Dashboard";
import Home from "views/Home/Home";
import UserProfile from "views/UserProfile/UserProfile";
// import TableList from "views/TableList/TableList";
import Transactions from "views/Transactions/Transactions"
import OffersContainer from "views/Offers/OffersContainer"
// import Typography from "views/Typography/Typography";
// import Icons from "views/Icons/Icons";
// import Maps from "views/Maps/Maps";
// import Notifications from "views/Notifications/Notifications";
// import Upgrade from "views/Upgrade/Upgrade";

const rcProjectRoutes = [
  {
    path: "/home",
    name: "Home",
    icon: "pe-7s-home",
    component: Home
  },
  {
    path: "/transactions",
    name: "Transactions",
    icon: "pe-7s-credit",
    component: Transactions
  },
  {
    path: "/offers",
    name: "Offers",
    icon: "pe-7s-ticket",
    component: OffersContainer
  },
  // {
  //   path: "/compensation",
  //   name: "Compensation",
  //   icon: "pe-7s-cash",
  //   component: UserProfile
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   icon: "pe-7s-note2",
  //   component: TableList
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "pe-7s-news-paper",
  //   component: Typography
  // },
  // { path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
  // { path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "pe-7s-bell",
  //   component: Notifications
  // },
  {
    upgrade: true,
    path: "/login",
    name: "Login",
    icon: "pe-7s-user",
    component: UserProfile
  },
  // {
  //   upgrade: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "pe-7s-rocket",
  //   component: Upgrade
  // },
  { redirect: true, path: "/", to: "/home", name: "Home" }
];

export default rcProjectRoutes;
