import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";
import NumberFormat from 'react-number-format';
import Card from "components/Card/Card.jsx";
import { TRANSACTION_LABELS } from "variables/Variables.jsx";
// import { connect } from 'react-redux';
// import { withRouter } from 'react-router-dom';
import { getTransactionList } from 'actions/index.js';
import { view } from 'react-easy-state';
import { store }  from '../../appStore.js';

export class Transactions extends Component {

  componentDidMount() {
    // console.log(store);
    // this.test();
    getTransactionList().then((response) => {
          console.log('inside api call');
          console.log(response);

          if(response.status !== 200){
            throw Error(response.statusText);
          }

          // dispatch(transactionsAreLoading(false))
          // console.log(store);
          response.data.transactions.map((transaction,index) => store.setTransactions({transaction}))
          console.log(store);
          // console.log(response.data.transactions);
          // if(data != null){
          //   data.map((t) => this.setTransactions(t) )
          // }
          // store.transactions = response.data.transactions;

          this.forceUpdate();
        })
        // .then((response)=> dispatch(transactionsFetchSuccess(response.data)))
        // .catch((e) => dispatch(transactionsHaveError(e, true)));
        .catch((e) => alert(e))
    // console.log('after api call');
    // console.log(data);

    // console.log(store);
    // console.log(this.getTransactions);
  }
  // test = () => {
  //   store.setEmployee('999');
  // }

  render() {
    const {
      getCustomer,
      getEmployeeId,
      getEmployeeName,
      setEmployee,
      getTransactions,
      transactions
    } = store


    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col>
              <Card
                title="Recent Transactions"
                category=""
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        {
                          TRANSACTION_LABELS.map((prop, key) => {
                            return (
                            <th  key={key}>{prop}</th>
                            );
                          })
                        }
                        </tr>
                    </thead>
                    <tbody>
                    {
                        store.transactions.map((prop,key) =>
                        {
                          var tmp = prop.transaction;
                          console.log('help');
                          console.log(tmp.transaction_id);
                          return(
                            <tr key={key}>
                              <td>{tmp.transaction_id}</td>
                              <td>{tmp.transaction_date}</td>
                              <td>{tmp.vendor}</td>
                              <td>{tmp.description}</td>
                              <td><NumberFormat value={tmp.amount} decimalScale={2} fixedDecimalScale={true} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>
                            </tr>

                            // console.log(prop);
                          )
                          // return prop.transaction_id;
                        })
                      }
                    </tbody>
                </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

/*
<h1>{this.jeff}</h1>
<h2>{this.transactionsToDisplay}</h2>
*/

// const mapStateToProps = (state) => {
//   return {
//       transactions: state.transactionsFetchSuccess,
//       hasError: state.transactionsHaveError,
//       isLoading: state.transactionsAreLoading
//   };
// };


// const mapDispatchToProps = (dispatch) => (
//     {
//       fetchData() {
//         dispatch(getTransactionList())
//       }
//     }
// );

export default view(Transactions);
