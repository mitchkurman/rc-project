import React, { Component } from "react";
import { FormGroup, InputGroup, FormControl  } from "react-bootstrap";
import { CustomButton } from "components/CustomButton/CustomButton.jsx";
import { view } from 'react-easy-state';
import { store } from '../../appStore.js';


export class CardNumberSearch extends Component {
  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount(){
    // store.customer = {"firstName":"Mitch", "lastName": "Kurman"}
    document.getElementById("cn-button").value = "";
  }

  handleClick(){
    var newCn = document.getElementById("cn-button").value;
    store.setCardnumber(newCn);
  }


  render() {
    const {
      getCustomer,
      getEmployeeId,
      getEmployeeName,
      customer,
      getCardnumber,
      setCardnumber
    } = store
    return (

          <FormGroup>
            <InputGroup>
              <FormControl type="text" />
              <InputGroup.Button>
                <CustomButton id="cn-button" bsStyle="primary" onClick={this.handleClick} fill>Submit Cardnumber</CustomButton>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>

    )}
}
export default view(CardNumberSearch);
