import { store as createStore } from 'react-easy-state'

// use 'appStore' instead of 'this' in the store methods to make them passable as callbacks
// const appStore = store({
//   customer: {},
//   async getCustomer (filter) {
//     appStore.isLoading = true
//     appStore.customer = {"firstName":"Rick","LastName":"James"}
//     appStore.isLoading = false
//   }
// })

export const store = createStore({
  customer: [],
  employee: {"employeeId":"123","employeeName":"Steve Employee"},
  transactions: [],
  isUpdated:false,
  offers: [],
  cardnumber: {},
  get getCustomer() {
    return store.customer;
  },
  get getEmployeeId(){
    return ''+store.employee.employeeId;
  },
  get getEmployeeName(){
    return ''+store.employee.employeeName;
  },
  setEmployee (employeeId) {
    store.employee.employeeId = employeeId;
    store.isUpdated = true;
  },
  get getTransactions(){
    console.log(store.transactions);
    console.log(store.transactions);
    return store.transactions;
  },
  setTransactions(transaction){
    // store.transactions.pop
    console.log(transaction);
    var dup = false;
    store.transactions.map((t,key ) => {
      console.log(t);
      if(t.transaction_id === transaction.transaction_id){
        dup = true;
      }
    })
    if(!dup){
      store.transactions.push(transaction);
    }
  },
  setCustomer(customer){
    console.log('about to set customer');
    console.log(store.customer);
    store.customer = JSON.parse(customer);
  },
  setOffers(offer){
    store.offers.pop();
    store.offers.push(offer);
    store.isUpdated = true;
  },
  get getOffers(){
    console.log(store.offers);
    return store.offers;
  },
  get getCardnumber(){
    return store.cardnumber;
  },
  setCardnumber(cn){
    store.cardnumber = cn;
  }
});

// export default appStore
