import { combineReducers } from 'redux';
import { transactionsFetchSuccess, transactionsHaveError, transactionsAreLoading } from './TransactionReducers';

export default combineReducers({
    transactionsFetchSuccess,
    transactionsHaveError,
    transactionsAreLoading
});
