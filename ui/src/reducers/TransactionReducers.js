export function transactionsHaveError(state = false, action) {
  console.log(action.type);
        switch (action.type) {
            case 'TRANSACTIONS_HAVE_ERROR':
                return action.hasError;
            default:
                return state;
        }
    }

export function transactionsAreLoading(state = false, action) {
    switch (action.type) {
        case 'TRANSACTIONS_ARE_LOADING':
            return action.isLoading;
        default:
            return state;
    }
}

export function transactionsFetchSuccess(state = {}, action) {
    switch (action.type) {
        case 'TRANSACTIONS_FETCH_DATA_SUCCESS':
            return action.data.transactions;
        default:
            return state;
    }
}
