import React, { Component } from "react";
import { Row, Col, Table, Grid } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx"
import Offers from "views/Offers/Offers"

export class OffersContainer extends Component {
  render(){
    return(
      <div className="content">
        <Grid fluid>
          <Row>
            <Col>
              <Offers />
            </Col>
          </Row>
          <Row>
            <Col>
            <Card
              title="Offer History"
              // category="Here is a subtitle for this table"
              ctTableFullWidth
              ctTableResponsive
              content={
                <Table striped hover>
                  <thead>
                    <tr>

                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </Table>
              }
            />
            </Col>
          </Row>
        </Grid>
      </div>

    )
  }
}
export default OffersContainer;
