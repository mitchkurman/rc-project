import request from 'superagent'
// const API_ROOT = 'https://f1cq2j22mk.execute-api.us-east-1.amazonaws.com/mock';
const Middleware = store => next => action => {
  /*
  Pass all actions through by default
  */
  next(action)
  switch (action.type) {
    case 'GET_TRANSACTION_LIST':
      /*
    In case we receive an action to send an API request, send the appropriate request
    */
      request.post('https://f1cq2j22mk.execute-api.us-east-1.amazonaws.com/mock/account/card/transactions')
      .send({})
      .set('Content-Type','application/json')
      .end((err, res) => {
        if (err) {
          /*
          in case there is any error, dispatch an action containing the error
          */
          return next({
            type: 'GET_TRANSACTION_LIST_ERROR',
            err
          })
        }
        const data = JSON.parse(res.text)
        // console.log(data);
        /*
        Once data is received, dispatch an action telling the application
        that data was received successfully, along with the parsed data
        */
        next({
          type: 'TRANSACTION_LIST_RECEIVED',
          data
        })
      })
      break
    /*
  Do nothing if the action does not interest us
  */
    default:
      break
  }
}

export default Middleware
