import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
// import { withRouter } from 'react-router-dom';

import { Card } from "components/Card/Card.jsx";
import { getOffer } from 'actions/index.js';
import { view } from 'react-easy-state';
import { store }  from '../../appStore.js';

export class Offers extends Component {

  componentDidMount() {
    getOffer("platinum","372545000000006").then((response) => {
          console.log('inside offers for platinum');
          console.log(response);

          if(response.status !== 200){
            throw Error(response.statusText);
          }

          // response.data.map((offer,index) => store.setOffers({offer}))
          store.setOffers(response.data);
           // console.log(store);
          console.log(store);

          this.forceUpdate();
        })
        .catch((e) => alert(e))
  }

  render() {
    console.log(store);
    const {
      getCustomer,
      getEmployeeId,
      getEmployeeName,
      customer,
      setOffers,
      offers
    } = store

    console.log(store.getOffers);
    return (
      <Card
        id="offerInfo"
        title="Current Offer"
        content={
          <div>
            <Row>
              <Col md={12} align="center">
              {
                  store.offers.map((prop,key) =>
                  {
                    console.log('about to render url');
                    console.log(prop);
                    // var tmp = JSON.parse(prop);
                    // console.log(prop.s3Url);
                    var tmp=JSON.parse(prop);
                    return(
                      <div key={key}>
                      <img alt='tmp' src={tmp.s3Url} width='100%' />
                      </div>
                    )
                  })
                }

              </Col>
            </Row>
          </div>
      }
    />
  )}
}
// export default withRouter(Offers);

export default Offers;
