import axios from 'axios'
import { store, setTransactions } from 'appStore.js'

export function getTransactionList(){
  console.log('getTransactionList123');
  // return (dispatch) => {

    // dispatch(transactionsAreLoading(true));
    return axios.post('https://f1cq2j22mk.execute-api.us-east-1.amazonaws.com/mock/account/card/transactions', {}, {
        headers: { 'content-type': 'application/json', 'Accept': 'application/json' }
    })
}

export function getCustomerInfo(cardnbr){
  console.log('getCustomerInfo');
  console.log('cardnumber: '+cardnbr);
  // return (dispatch) => {

    // dispatch(transactionsAreLoading(true));
    return axios.post('https://f1cq2j22mk.execute-api.us-east-1.amazonaws.com/prod/account/customer/demographics', {cardnumber:cardnbr}, {
        headers: { 'content-type': 'application/json', 'Accept': 'application/json' }
    })
}

export function getOffer(status,cardnbr){
  console.log('getOffer');
  // return (dispatch) => {

    // dispatch(transactionsAreLoading(true));
    return axios.post('https://f1cq2j22mk.execute-api.us-east-1.amazonaws.com/prod/offers', {customer_status:status,cardnumber:cardnbr}, {
        headers: { 'content-type': 'application/json', 'Accept': 'application/json' }
    })
}

// export function transactionsFetchSuccess(data){
//   return {
//     type: 'TRANSACTIONS_FETCH_DATA_SUCCESS',
//     data
//   };
// }
//
// export function transactionsHaveError(e, bool) {
//   console.log("transactionsHaveError: " + bool);
//   console.log(e);
//     return {
//         type: 'TRANSACTIONS_HAVE_ERROR',
//         hasError: bool
//     };
// }
//
// export function transactionsAreLoading(bool) {
//     return {
//         type: 'TRANSACTIONS_ARE_LOADING',
//         isLoading: bool
//     };
// }
