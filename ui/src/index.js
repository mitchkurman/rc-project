import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { HashRouter, Route, Switch } from "react-router-dom";
import indexRoutes from "routes/index.jsx";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/sass/light-bootstrap-dashboard.css?v=1.2.0";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";
// import { createStore, applyMiddleware, compose } from 'redux'
// import thunk from 'redux-thunk'
// import combineReducers from "./reducers/index.js"

// let store = createStore(combineReducers, {} ,compose(
//    applyMiddleware(thunk),
//    window.devToolsExtension ? window.devToolsExtension() : f => f
//   )
// )

ReactDOM.render(
  // <Provider store={store}>
    <HashRouter>
      <Switch>
        {indexRoutes.map((prop, key) => {
          return <Route to={prop.path} component={prop.component} key={key} />;
        })}
      </Switch>
    </HashRouter>
  // </Provider>
  ,
  document.getElementById("root")
);

// store.dispatch({ type: 'GET_TRANSACTION_LIST' })
